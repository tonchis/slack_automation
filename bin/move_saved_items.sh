#! /usr/bin/env bash

##
# The _raison d'etre_ for this script is to make my task management workflow in Slack easier.
#
# Many of my action items start with a Slack message so to keep track of them I used to
# save them in Saved Items. Then that list became my "to do" list, so I can follow up on work.
#
# However clever this was, the Saved Items list is lacking in terms of task management. I can't
# triage items, prioritize them or anything like that. Old items were often forgotten. So I
# moved my task list to Airtable
#
# This script fetches my Slack Saved Items and moves each one into Airtable, leveraging both APIs.
#
# It's intended to be used with something like crontab, so here's the entry I'm using:
# 
#     */5 * * * * env \
#       JQ_BIN=/opt/homebrew/bin/jq \
#       USER_TOKEN_FILE=/path/to/file \
#       AIRTABLE_USER_TOKEN_FILE=/path/to/file \
#       ~/code/slack_automation/bin/move_saved_items.sh > ~/code/slack_automation/log/cron.log
#
# It'll run every 5 minutes. Please, note that to set all these env variables you need the `env`
# utility because crontab doesn't run scripts in a login shell so your custom $PATH isn't loaded.

# Abort the script if errors are raised.
set -e

# When running this script from crontab or Alfred Workflows it will not load the PATH
# the regular shell has, so we pass the path to jq as an env variable.
jq_bin=$([[ -n `command -v jq` ]] && echo "jq" || echo "$JQ_BIN")

# Fetch all the saved items for my Slack user but keep only
# those that are messages (exclude starred channels, for example).
#
# We want to iterate over them, but because the text may contain spaces
# or newlines, we base64 encode each item. This wasn't my idea, I'm not
# that clever. See the link below:
# https://www.starkandwayne.com/blog/bash-for-loop-over-json-array-using-jq/
#
# A few more notes about the jq call below:
# * -c compacts the output and puts each element in a newline
# * -r outputs raw text instead of JSON, to remove double quotes
# * the trailing '| .[]' prints each array element instead of the array object
#
# TODO: Support pagination.
#
saved_items=$(curl -s -X POST 'https://slack.com/api/stars.list' \
  -H "Authorization: Bearer $(cat $USER_TOKEN_FILE)" \
  -H 'Content-Type: application/json; charset=utf-8' \
  | $jq_bin -c -r '.items | map(select(.type | contains("message")) | @base64) | .[]'
)

echo ${saved_items}

for row in $(echo ${saved_items}); do
  # For each encoded newline we bind the _jq function to that row
  # to decode it and send it through the real jq.
  _jq() {
    echo ${row} | base64 --decode | $jq_bin -r ${1}
  }
  echo "SAVED ITEM:" $(_jq '.message.text')
  echo $(_jq '.')
  echo

  # Save the message in Airtable.
  # TODO: Move to a script of its own.
  #
  text=$(echo $(_jq '.message.text'))
  permalink=$(echo $(_jq '.message.permalink'))
  new_task=$(curl -X POST https://api.airtable.com/v0/apprttvOEZeowaBAU/Work \
    -H "Authorization: Bearer $(cat $AIRTABLE_USER_TOKEN_FILE)" \
    -H "Content-Type: application/json" \
    --data "{
    \"records\": [
      {
        \"fields\": {
          \"Name\": \"$(echo $text | cut -d ' ' -f1,2,3,4,5)\",
          \"Description\": \"$text\",
          \"Slack\": \"$permalink\",
          \"Priority\": \"This Week\"
        }
      }
    ]
  }")
  echo "NEW TASK"
  echo $new_task
  echo

  # Remove the message from Saved Items.
  # TODO: Only execute this if the script to save into Airtable runs successfully.
  #
  timestamp=$(echo $(_jq '.message.ts'))
  channel=$(echo $(_jq '.channel'))
  delete_task=$(curl -s -X POST 'https://slack.com/api/stars.remove' \
    -H "Authorization: Bearer $(cat $USER_TOKEN_FILE)" \
    -H 'Content-Type: application/json; charset=utf-8' \
    -d "{\"channel\": \"$channel\", \"timestamp\": \"$timestamp\"}")
  echo "DELETE TASK"
  echo $delete_task
  echo
done


