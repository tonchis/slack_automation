#! /usr/bin/env bash

set -e

args="${@:1}"
text=$([[ -n $args ]] && echo "$args" || echo $DEFAULT_TEXT)

# The channels are passed as the CHANNELS env variable concatenated by colon.
# CHANNELS='#foo:#bar:#baz'
IFS=":"
for channel in $CHANNELS; do
  curl -s -X POST 'https://slack.com/api/chat.postMessage' \
    -H "Authorization: Bearer $(cat $USER_TOKEN_FILE)" \
    -H 'Content-Type: application/json; charset=utf-8' \
    -d "{\"channel\": \"$channel\", \"text\": \"$text\"}"
done
