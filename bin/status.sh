#! /usr/bin/env bash

set -e

emoji=$1
emoji=$([[ -n $emoji ]] && echo $emoji || echo "")

expiration=$([[ -n $2 ]] && echo $(($(date +%s) + $2 * 60)) || echo 0)

curl -s -X POST 'https://slack.com/api/users.profile.set' \
  -H "Authorization: Bearer $(cat $USER_TOKEN_FILE)" \
  -H 'Content-Type: application/json; charset=utf-8' \
  -d "{\"profile\": {\"status_emoji\": \"$emoji\", \"status_text\": \"\", \"status_expiration\": $expiration}}"
