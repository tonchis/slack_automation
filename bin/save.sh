#! /usr/bin/env bash

set -e

text="${@:1}"

res=$(curl -s -X POST 'https://slack.com/api/chat.postMessage' \
  -H "Authorization: Bearer $(cat $USER_TOKEN_FILE)" \
  -H 'Content-Type: application/json; charset=utf-8' \
  -d "{\"channel\": \"$AT_ME\", \"text\": \"$text\"}")

# When running this script from Alfred Workflows it will not load the PATH
# the regular shell has, so we pass the path to jq as an env variable.
jq_bin=$([[ -n `command -v jq` ]] && echo "jq" || echo "$JQ_BIN")

channel_id=$(echo $res | $jq_bin .channel)
message=$(echo $res | $jq_bin .ts)

curl -s -X POST 'https://slack.com/api/stars.add' \
  -H "Authorization: Bearer $(cat $USER_TOKEN_FILE)" \
  -H 'Content-Type: application/json; charset=utf-8' \
  -d "{\"channel\": $channel_id, \"timestamp\": $message}"
