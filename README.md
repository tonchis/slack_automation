# Slack Automation

This repo is just a collection of shell scripts to use the Slack API for some basic automation of my daily workflow.

## Authentication

To function they all need a User OAuth Token. To get one you'll need to create a Slack App and give it the following OAuth user scopes:

* [`chat:write`](https://api.slack.com/scopes/chat:write) to send Slack messages on your behalf.
* [`users:write`](https://api.slack.com/scopes/users:write) to set the active/away status.
* [`users.profile:write`](https://api.slack.com/scopes/users.profile:write) to set the custom status, as if you were using the `/status` command in Slack.

After the Workspace Admins approve your app, you should be able to see a User OAuth Token in the _OAuth & Permissions_ section of your app. Place this token in a file of your choosing.

Minor note: User Tokens start with the `xoxp-` prefix, not to be confused with the `xoxb-` tokens, which are for Slack Bots.

To use the scripts below you'll need to set the `USER_TOKEN_FILE=/path/to/file` environment variable.

## Scripts

The `bin/` directory contains the scripts that interact with the Slack API.

### `say.sh`

This script will send the same message to multiple channels. It requires the following environment variables:

* `USER_TOKEN_FILE`: the path to the file with the User OAuth Token of your Slack App.
* `CHANNELS`: a colon-separated list of Slack channels where to broadcast the message: `CHANNELS='#foo:#bar:#baz'`.
* `DEFAULT_TEXT`: a string to send as the default message.

For example:
```bash
$ USER_TOKEN_FILE=/path/to/file CHANNELS='#foo:#bar:#baz' DEFAULT_TEXT=hi ./bin/say.sh
```

Or

```bash
$ USER_TOKEN_FILE=/path/to/file CHANNELS='#foo:#bar:#baz' DEFAULT_TEXT=hi ./bin/say.sh goodbye
```

### `status.sh`

This script will set your Slack status to an emoji of your liking. The status text will always be empty.

You will need the following environment variables:

* `USER_TOKEN_FILE`: the path to the file with the User OAuth Token of your Slack App.
* `DEFAULT_STATUS_EMOJI`: a string for the default emoji, like `:wave:`.

This will set :wave: as your status
```bash
$ USER_TOKEN_FILE=/path/to/file DEFAULT_STATUS_EMOJI=':wave:' ./bin/status.sh
```

Or

```bash
$ USER_TOKEN_FILE=/path/to/file DEFAULT_STATUS_EMOJI=':wave:' ./bin/status.sh :brb:
```

You can also pass a second argument to indicate the expiration of the status, in minutes.

```bash
$ USER_TOKEN_FILE=/path/to/file DEFAULT_STATUS_EMOJI=':wave:' ./bin/status.sh :brb: 30
```

Using `clear` as the argument or default will clear your status.

```bash
$ USER_TOKEN_FILE=/path/to/file DEFAULT_STATUS_EMOJI=':wave:' ./bin/status.sh clear
```

## Integrations

### Alfred

I use [AlfredApp](https://www.alfredapp.com) in MacOS which has a neat Workflows feature that let you automate all sorts of things.

I set up a workflow to call each script from a command (as an _External Script_), and configured all the environment variables from the _Workflow Environment Variables_ panel (in the top-right `[X]` icon).

This way I can quickly broadcast a message or set my status from anywhere.

### Shortcuts

Another integration I was playing with is the Shortcuts app in iPhone. Thanks to [this post](https://alexrepty.com/2020/03/03/automating-slack-status-using-nfc-tags/) that explains how to leverage NFC tags in iPhone I learned that the iOS Shortcuts app lets you run scripts over SSH in your computer. I know, right?!

It's not very hard to put together. This [other post](https://matsbauer.medium.com/how-to-run-ssh-terminal-commands-from-iphone-using-apple-shortcuts-ssh-29e868dccf22) explains the step by step, but I basically hooked up Shortcuts to call a new `~/bin/slack_lunch` script with all the environment variables I need to set my lunch status in Slack. Now, as I go out to buy lunch I don't have to go back to the keyboard when I realized I forgot to set my status. I can simply hit a shortcut on my phone or Watch at the door. What a world we live in, huh?

